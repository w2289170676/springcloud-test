package com.mall.gateway.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;


/**
 * @Description: 自定义拦截器
 * @Auther: lizz
 * @Date: 2019/12/26 17:32
 */
@Component
public class JWTFilter implements GlobalFilter, Ordered {
    private static final Logger logger = LoggerFactory.getLogger(JWTFilter.class);
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        logger.info(" JWTFilter 过滤");
        //获取请求信息进行过滤
//        exchange.getApplicationContext();
//        exchange.getAttribute("uid");
//        exchange.getAttributes();
//        exchange.getFormData();
//        exchange.getRequest().getQueryParams();
//        exchange.getSession();
        //拦截请求
        //return exchange.getResponse().setComplete();
        //执行下一个拦截器
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        //拦截器优先级，0位最先执行
        return 0;
    }
}