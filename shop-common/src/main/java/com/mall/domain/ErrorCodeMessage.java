package com.mall.domain;

public interface ErrorCodeMessage {
    Integer getcode();
    String getMessage();
}
