package com.mall.domain.exception;

import com.mall.domain.ErrorCodeMessage;
import lombok.Data;

@Data
public class BaseException extends RuntimeException {
    private Integer code;
    private String message;
    public BaseException(ErrorCodeMessage errorCodeMessage){
        this.code = errorCodeMessage.getcode();
        this.message = errorCodeMessage.getMessage();
    }

    public BaseException(Integer code, String message){
        this.code = code;
        this.message = message;
    }
}
