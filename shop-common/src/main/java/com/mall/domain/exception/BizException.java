package com.mall.domain.exception;

import com.mall.domain.ErrorCodeMessage;
import lombok.Data;

public class BizException extends BaseException{

    public BizException(ErrorCodeMessage errorCodeMessage){
        super(errorCodeMessage);
    }
}
