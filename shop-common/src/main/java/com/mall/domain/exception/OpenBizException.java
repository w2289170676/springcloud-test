package com.mall.domain.exception;

import com.mall.domain.ErrorCodeMessage;
import lombok.Data;

public class OpenBizException extends BaseException{
    public OpenBizException(ErrorCodeMessage errorCodeMessage){
        super(errorCodeMessage);
    }

    public OpenBizException(Integer code, String message){
        super(code, message);
    }
}
