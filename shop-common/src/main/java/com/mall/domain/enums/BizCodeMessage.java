package com.mall.domain.enums;

import com.mall.domain.ErrorCodeMessage;

public enum BizCodeMessage implements ErrorCodeMessage {

    DEFAULT_ERROR(10010, "服务器错误"),
    NOT_FOUNT_ERROR(10010, "资源找不到"),
    USERNAME_PASSWORD_NOT_FOUND_ERROR(10011, "用户名或者密码错误"),
    HH(10012, "haha"),
    TEST_ERROR(10013, "测试错误");

    private Integer code;
    private String message;

    BizCodeMessage(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public Integer getcode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
