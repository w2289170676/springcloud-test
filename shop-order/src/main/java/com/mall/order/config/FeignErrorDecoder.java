package com.mall.order.config;

import com.alibaba.fastjson.JSONObject;
import com.mall.domain.Result;
import com.mall.domain.enums.BizCodeMessage;
import com.mall.domain.exception.BaseException;
import com.mall.domain.exception.OpenBizException;
import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


/**
 * feign异常处理
 * @author:JZ
 * @date:2020/5/16
 */
@Slf4j
//@Component
public class FeignErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String s, Response response) {
        try {
            String errorContent = Util.toString(response.body().asReader());
            Result result = JSONObject.parseObject(errorContent, Result.class);
            Integer status = result.getCode();
            String message = result.getMessage();
            log.info(errorContent);
            return new OpenBizException(status, message);
        } catch (Exception e) {
            log.error("处理FeignClient 异常错误");
            return new OpenBizException(BizCodeMessage.DEFAULT_ERROR);
        }
    }

}
